import Navbar from './components/Navbar';
import Hero from './components/Hero';
import Analitics from './components/Analitics';
import Newsletter from './components/Newsletter';
import Cards from './components/Cards';
import Footer from './components/Footer';

function App() {
  return (
    <div>
      <Navbar />
      <Hero />
      <Analitics />
      <Newsletter />
      <Cards />
      <Footer />
    </div>
  );
}

export default App;
